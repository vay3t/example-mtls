package main

import (
	"crypto/tls"
	"crypto/x509"
	_ "embed"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

var (
	//go:embed certs/ca.crt
	caCert []byte
	//go:embed certs/client.crt
	clientCert []byte
	//go:embed certs/client.key
	clientKey []byte
)

func main() {

	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)

	certificate, err := tls.X509KeyPair(clientCert, clientKey)
	if err != nil {
		log.Fatalf("could not load certificate: %v", err)
	}

	client := http.Client{
		Timeout: time.Second * 30,
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				RootCAs:      caCertPool,
				Certificates: []tls.Certificate{certificate},
			},
		},
	}

	url := "https://localhost:8443/hello"
	r, err := client.Get(url)
	if err != nil {
		log.Fatalf("error making get request: %v", err)
	}

	defer r.Body.Close()
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatalf("error reading response: %v", err)
	}

	fmt.Printf("%s\n", body)
}
